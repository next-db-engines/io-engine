# Next DB
First microservice baseed DBMS

# Clone
##### linux:
```sh
git clone --branch linux https://gitlab.com/next-db-engines/io-engine.git
```

##### Windows:
```sh
git clone --branch windows https://gitlab.com/next-db-engines/io-engine.git
```


# Run
```sh
io-engine <natshost:port> <server_id>
```
